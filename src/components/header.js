import PropTypes from 'prop-types'
import React from 'react'

import { Link } from 'gatsby'
import styled from 'react-emotion'

import Icon from '../icon'

const Wrapper = styled('div')`
  height: ${props => props.theme.topbarHeight}px;
  width: 100%;
  position: fixed;
  display: flex;
  align-items: center;
  padding: 0.625rem;
  background: rgba(255, 255, 255, .9);
`

const Title = styled(Link)`
  margin-left: 0.625rem;
  color: ${props => props.theme.colors.primary};
  font-size: 0.75rem;
  text-decoration: none;
`

function Header(props) {
  return (
    <Wrapper>
      <Icon name="globe" size="small" />
      <Title to="/">{props.siteTitle}</Title>
    </Wrapper>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string.isRequired
}

export default Header
