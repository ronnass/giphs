import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import styled from 'react-emotion'

import Header from './header'
import './layout.css'

const Body = styled('div')`
  height: 100vh;
  width: 100vw;
  padding: calc(10px + ${props => props.theme.topbarHeight}px) 1.0875rem 1.45rem;
  background: linear-gradient(#e2f9cd, #cae3ff);
  background-attachment: fixed;
  overflow-y: scroll;
`

function Layout(props) {
  return (
    <StaticQuery
      query={graphql`
        query SiteTitleQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
      render={data => (
        <>
          <Helmet
            title={data.site.siteMetadata.title}
            meta={[
              { name: 'description', content: 'Sample' },
              { name: 'keywords', content: 'sample, something' },
            ]}
          >
            <html lang="en" />
          </Helmet>
          <Header siteTitle={data.site.siteMetadata.title} />
          <Body>{props.children}</Body>
        </>
      )}
    />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
