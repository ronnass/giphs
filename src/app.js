import PropTypes from 'prop-types'
import React from 'react'
import { Provider } from 'react-redux'
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import effects from 'redux-effects'
import fetch, { fetchEncodeJSON } from 'redux-effects-fetch'
import { ThemeProvider } from 'emotion-theming'

import theme from './theme'

import { reducer } from './reducer'

const middleware = applyMiddleware(effects, fetch, fetchEncodeJSON, thunk)
const store = createStore(reducer, middleware)

function App(props) {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        {props.children}
      </Provider>
    </ThemeProvider>
  )
}

App.propTypes = {
  children: PropTypes.object.isRequired
}

export default App
