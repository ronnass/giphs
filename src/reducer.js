import * as R from 'ramda'
import { bind } from 'redux-effects'
import { fetch } from 'redux-effects-fetch'

const config = {
  api: 'http://api.giphy.com/v1/gifs',
  key: 'jMw4vDC4TnxdT3WZS1MxKlhDefLY164z'
}

const INITIAL_STATE = {
  gifs: []
}

export const UPDATE = 'GIPHLAR/UPDATE'

export function update(payload) {
  return {
    type: UPDATE,
    payload,
  }
}

export function fetchGifs() {
  return bind(fetch(`${config.api}/trending?api_key=${config.key}`, {
    method: 'GET',
  }), ({value}) => update(value.data), ({value}) => new Error('Fetching failed: ' + value))
}

export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE: {
      return R.merge(state, { gifs: action.payload })
    }

    default: {
      return state
    }
  }
}

export function getGifs(state) {
  return  R.propOr('', 'gifs', state)
}
